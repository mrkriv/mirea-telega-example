#Build stage
FROM gradle:latest AS BUILD
WORKDIR /usr/app/
COPY . .

RUN gradle build -xtest


# Package stage
FROM amazoncorretto:17
ENV APP_HOME=/usr/app/

WORKDIR $APP_HOME
COPY --from=BUILD $APP_HOME/build/libs/demo2-0.0.1-SNAPSHOT.jar /usr/app/app.jar

EXPOSE 8080
