package com.example.demo.rest

import com.example.demo.dto.BotAppUser
import com.example.demo.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("users")
class UserController(var userService: UserService) {

    @PostMapping("")
    fun addUser(@RequestBody user: BotAppUser) {
        userService.addUser(user)
    }

    @GetMapping("search/{userName}")
    fun addUser(@PathVariable userName: String): BotAppUser? {
        return userService.getUserByName(userName)
    }
}