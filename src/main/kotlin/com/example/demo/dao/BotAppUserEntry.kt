package com.example.demo.dao

import jakarta.persistence.*

@Entity
@Table(name = "USERS")
class BotAppUserEntry {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    var name: String = ""

    var isAdmin: Boolean = false
}
