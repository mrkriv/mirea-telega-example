package com.example.demo.service

import com.example.demo.dao.BotAppUserEntry
import com.example.demo.dto.BotAppUser
import com.example.demo.repo.UserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
class UserService(var userRepository: UserRepository) {
    fun getUser(userId: Long): BotAppUser? {
        var dbUser = userRepository.findByIdOrNull(userId)
        if (dbUser?.id == null)
            return null;

        return BotAppUser(dbUser.id!!, dbUser.name, dbUser.isAdmin)
    }

    fun getUserByName(userName: String): BotAppUser? {
        var dbUser = userRepository.findByNameOrNull(userName).get()
        if (dbUser?.id == null)
            return null;

        return BotAppUser(dbUser.id!!, dbUser.name, dbUser.isAdmin)
    }

    fun addUser(user: BotAppUser) {
        var dbUser = BotAppUserEntry();
        dbUser.id = user.id
        dbUser.name = user.name
        dbUser.isAdmin = user.isAdmin

        userRepository.save(dbUser)
    }
}