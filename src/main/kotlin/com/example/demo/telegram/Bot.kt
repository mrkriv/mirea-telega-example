package com.example.demo.telegram

import com.example.demo.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import java.util.*

@Component
class Bot(telegramBotsApi: TelegramBotsApi, var config: BotProperties, var userService: UserService) :
    TelegramLongPollingBot() {
    private var latitude: Double = .0
    private var longitude: Double = .0
    private var timeOfPreviousPosition: Date? = null

    private val log: org.slf4j.Logger = LoggerFactory.getLogger(Bot::class.java)

    enum class BotAnswer(val message: String) {
        START("Welcome to AOMS bot!"),
        VOICE("Your voice message duration: "),
        CURRENT_LOCATION("Your current position "),
        CHANGE_LOCATION("Your position changed on "),
        DEFAULT("Unknown command")
    }

    init {
        telegramBotsApi.registerBot(this)
    }

    override fun getBotUsername(): String = config.username

    override fun getBotToken(): String = config.token

    override fun onUpdateReceived(update: Update) {
        if (!update.hasMessage())
            return

        val sendMessage = SendMessage().also { it.setChatId(update.message.chatId) }

        log.info("Message from ${update.message.from.id} (${update.message.from.userName}): ${update.message.text}")

        val user = userService.getUser(update.message.from.id)
        if (user == null) {
            sendMessage.also {
                it.text = "..."
                execute(it)
            }
            return
        }

        if (update.message.hasText()) {
            sendMessage.also {
                it.text = when (update.message.text) {
                    "/start" -> BotAnswer.START.message
                    else -> BotAnswer.DEFAULT.message
                }
                execute(it)
            }
        } else if (update.message.hasVoice()) {
            sendMessage.also {
                it.text = BotAnswer.VOICE.message + update.message.voice.duration
                execute(it)
            }
        } else if (update.message.hasLocation()) {
            if (timeOfPreviousPosition != null) {
                sendMessage.also {
                    it.text = BotAnswer.CHANGE_LOCATION.message + getCoordinates(
                        latitude - update.message.location.latitude,
                        longitude - update.message.location.longitude
                    ) + "       ${timeOfPreviousPosition}"
                    execute(it)
                }
            }

            timeOfPreviousPosition = Calendar.getInstance().time
            latitude = update.message.location.latitude
            longitude = update.message.location.longitude

            sendMessage.also {
                it.text = BotAnswer.CURRENT_LOCATION.message + getCoordinates()
                execute(it)
            }
        }
    }

    private fun getCoordinates(x: Double = latitude, y: Double = longitude): String = "($x; $y)"
}
