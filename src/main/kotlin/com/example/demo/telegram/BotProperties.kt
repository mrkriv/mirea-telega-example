package com.example.demo.telegram

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Component
@Configuration
@ConfigurationProperties(prefix = "bot")
class BotProperties {
    var username: String = "AOMSwithBot"
    var token: String = "---"
}
