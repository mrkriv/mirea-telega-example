package com.example.demo.repo

import com.example.demo.dao.BotAppUserEntry
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : CrudRepository<BotAppUserEntry, Long> {
    @Query("select b from BotAppUserEntry b where b.name = ?1")
    fun findByNameOrNull(name: String): Optional<BotAppUserEntry>

    fun findAllByIsAdmin(isAdmin: Boolean): Iterable<BotAppUserEntry>
    fun findAllByName(name: String): Iterable<BotAppUserEntry>
}